package com.example.ppe3_webview;

        import android.app.Activity;
        import android.app.Dialog;
        import android.content.Context;
        import android.net.ConnectivityManager;
        import android.net.NetworkInfo;
        import android.os.Bundle;
        import android.os.Handler;
        import android.os.Looper;
        import android.util.Log;
        import android.view.KeyEvent;
        import android.webkit.WebResourceRequest;
        import android.webkit.WebResourceResponse;
        import android.webkit.WebSettings;
        import android.webkit.WebView;

public class MainActivity extends Activity {

    private WebView webView = null;
    boolean timeout = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.webView = (WebView) findViewById(R.id.webview);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        WebViewClientImpl webViewClient = new WebViewClientImpl(this);
        webView.setWebViewClient(webViewClient);

        String baseUrl    = "http://10.10.9.143/asetar08-1/Asetar08/";
        String data       = "Relative Link";
        String mimeType   = "text/html";
        String encoding   = "UTF-8";
        String historyUrl = "http://10.10.9.143/asetar08-1/Asetar08/";

        webView.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, historyUrl);
        if(isNetworkAvailable()){
            webView.loadUrl("http://10.10.9.143/asetar08-1/Asetar08/");

        }else {
            webView.loadUrl("file:///android_asset/index.html");
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.webView.canGoBack()) {
            this.webView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}